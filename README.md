# Embedded-C

## This project

This document is my working folder as I am going throught the following online
courses on embedded system programming :

* [Microcontroller Embedded C Programming: absolute beginners](https://www.udemy.com/course/microcontroller-embedded-c-programming/) (done)
* [Mastering Microcontroller with Embedded Driver
  Development](https://www.udemy.com/course/mastering-microcontroller-with-peripheral-driver-development/) (In progress)
* [Mastering Microcontroller : TIMERS, PWM, CAN, RTC,LOW POWER ](https://www.udemy.com/course/microcontroller-programming-stm32-timers-pwm-can-bus-protocol)

I am using [STM32 Nucleo-64 development board with STM32F401RE
MCU](https://www.st.com/en/evaluation-tools/nucleo-f401re.html)


