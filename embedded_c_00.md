#Notes


#Section 3
## 18
When you give a .c file to a compiler, a few things is going to happen. To
help us understand what happens, we can pass an argument to the compiler so 
every temporary file is saved so we see what is happening.

```sh
gcc -save-temps helloworld.c
```

By adding `-save-temps` you get something like this :

```sh
ls -l
-rw-r--r-- 1   142 Jun 23 13:43  helloworld.c                                                                                                                                           
-rw-r--r-- 1 15823 Jun 23 13:43  helloworld.i                                                                                                                                        
-rw-r--r-- 1  1768 Jun 23 13:43  helloworld.o   
-rw-r--r-- 1   824 Jun 23 13:43  helloworld.s
```

Without going in to much details, here are the basic steps that 
the compiler goes through :

* A pre-processing stage, where all the pre-processing stages are going to be resolved. For instance, the `include` headers are actually added to our 
c code from the .c file. The resulting file is the .i file.

* The code generation stade where the c code is converted in assembly. The
  resulting file is the .s file

* The assembler stage, where the compile convert the assembly code to machine
  code, resulting in the .o file (object file)


# Section 4
## 25 : Notes on data types
Do not take data types memory size for granted : it is defined by the compiler,
not by the language. It is important to note that some is defined by the
compiler : char is always 1 byte, short is always 2 bytes and long long is
always 8 bytes
## 26 : The char datatype
Its just another type of integer datatype of 1 byte but is often used to
represent ASCII character
## 33 : The extern keyword
The `extern` keyword tells the compiler that the variable is defined somewhere
else so it doest not have to allow memory for that variable

# Section 6 
## 40 : Uses of the static keyword

* Managing variables visibility in multiple files 

Lets say I have a global variable in a file. I don't want this variable to be
accessed from another file (using the extern keyword). The solution is to set
it static and It wont be possible to access it from outside the file.

The same is true with functions.

# Section 7
## 48 Typecasting
### The Default data type
The "Default data type" for the compiler is a int. Lets say we have :
c```
unsigned char data = 0x87 + 0xFF00;
```

One might think that 0x87 is a char (one byte data) but when called like in
the example above it is treated as a 4 byte data

# Section 8
## 50 : ITM and the SWD protocol  

On the Arm Processors (M3+) there's a unit called the Instrumentation Trace
Macrocell unit (ITM unit) that supports debbuging (useful for GDB and the use
of call like printf.

The communication with that unit is done via a protocol called the Serial Wire
Debug protocol (SWD protocal )
