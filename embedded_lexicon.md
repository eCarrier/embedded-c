[[_TOC_]]

# This document
This document is a big ass lexicon of common and useful notions in embedded
system programming with a quick overview of what it mean/does. It does not aim to do a deep
dive on the subject but to do a quick overview and give relevant sources on the
subject.

It is mainly aimed towards C software developper on STM32 platforms as a quick
reference or when learning embedded software development.

---

# Micro Controller Units (MCU)
A Microcontroller is essentially a computer integrated in
[IC](#integrated-circuit-ic) that  is
programmable and used to control a system and perform automated task from various input and able to
generate multiple outputs.

There are 5 internal components in a microcontroller : 
* A [CPU](#central-processing-unit-cpu)
* The [peripherals](#peripheral)
* An array of different [memory types](#memories)
* A [clock](#clock)
* A [bus](#bus)

*Sources :*
* An [Introduction to
  Microcontrollers](https://www.arrow.com/en/research-and-events/articles/engineering-basics-what-is-a-microcontroller)
* The [Microcontroller
  definition](https://internetofthingsagenda.techtarget.com/definition/microcontroller) on [IoT Agenda](https://internetofthingsagenda.techtarget.com/definition/microcontroller)

## Central Processing Unit (CPU)
This is one of the most significant part of the MCU that is tasked with
actually executing the various instructions given by a program.

*Sources :*
* [Different parts of a
  CPU](https://www.tutorialspoint.com/computer_fundamentals/computer_cpu.htm)
* [Crash Course Computer Science on the
  CPU](https://www.youtube.com/watch?v=FZGugFqdr60)

## Peripheral
### Interrupt Service Routine (ISR)
## Memories
### Volatile memory
#### Data memory
The data memory is a memory space used at runtime to temporarely store data
from your program's execution. In an STM32 microcontroller, the data memory
uses [SRAM](#static-random-access-memory-sram)

*Sources :*
* [Microcontroller Memory
Organization](https://www.electronicshub.org/8051-microcontroller-memory-organization)
#### Static Random Access Memory (SRAM)
### Non-volatile memory
#### Program Memory
The program memory is a non-volatile memory (data won't be destroyed when you
remove current) that is used to store the instructions for the CPU.

In an STM32 microcontroller, the data memory uses
 [Flash Memory](#flash-memory)


*Sources :*
* [Microcontroller Memory
Organization](https://www.electronicshub.org/8051-microcontroller-memory-organization)

#### Read-Only Memory (ROM)
##### Mask Programmable ROM (MPROM)
The Mask Programmable ROM is a type of memory that when you write to it you
can't write again on it.

##### Ultraviolet Erasable Programmable ROM (EPROM)
The Ultraviolet Erasable Programmable ROM is a type of ROM that you can write
and erase, but it's very hard to reprogram as you have to remove it from the
chip and expose it to Ultraviolet to erase it 

##### Electrically Erasable Programmable ROM (EEPROM)
This one of the most popular non volatile erasable memory in the market right
now, as it is possible to erase it with simple current.

The write/erase cycle on the EEPROM works on bit instead of bytes so it is a
bit slower than FLASH

*Sources :*
[Reading and writing to EEPROM](https://learn.sparkfun.com/tutorials/reading-and-writing-serial-eeproms/reading-and-writing)
* [Good stack exchanges
post](https://electronics.stackexchange.com/questions/65503/why-would-one-still-use-normal-eeprom-instead-of-flash)
on EEPROM vs Flash Memory
* [Good explanation on stack exchange](https://electronics.stackexchange.com/questions/201707/primary-diff-between-eeprom-and-flash-mem-in-terms-of-code-memory)

#### Flash memory

The flash memory is another type of EEPROM but its write/erase cycle works on
larger chunks of data compared to the typical EEPROM. It is also cheaper than
EEPROM but have a shorter life an durability.

*Sources :*
* [How Flash Memory Works](https://www.youtube.com/watch?v=g_2lr8z852M)
* [Good stack exchanges
post](https://electronics.stackexchange.com/questions/65503/why-would-one-still-use-normal-eeprom-instead-of-flash)
on EEPROM vs Flash Memory
* [Good explanation on stack exchange](https://electronics.stackexchange.com/questions/201707/primary-diff-between-eeprom-and-flash-mem-in-terms-of-code-memory)

#### Ferroelectric Random Access Memory (FRAM)
**TODO**
#### On Time Programmable (OTP) 
**TODO**


## Clock
### Peripheral clocks
### The Crystal Oscillators
### The RC Oscillators
### The Phase Locked Loop (PLL)
### High Speed Internal (HSI)
### High Speed External (HSE) 
### Microcontroller clock output (MCO)
## Bus
### Advanced High-Performance Bus (AHB)
#### AHB-Lite bus
### Advanced Peripheral Bus (APB)
### The Address Bus
### The Data Bus
### The Control Bus
### The Instruction Bus
### The System Bus
## Prescalers
## Nested vectored interrupt controller (NVIC) 
## Vector table
## 


---

# ARM
## Components
### The Instrumentation Trace Macrocell (ITM) unit
On the Arm Processors (Cortex-M3 and foward) there's a unit called the Instrumentation Trace
Macrocell unit (ITM unit) that supports debbuging (useful for GDB and the use
of call like printf.)

*Sources :*
[ARM ITM: Why you need a fast debug
probe!](https://percepio.com/2016/06/09/arm-itm/)

### The Serial Wire Debug (SWD) connector
Is a connection type used while using the SWD protocol for debugging.
See [The SWD protocol](#the-serial-wire-debug-swd-protocol)

### General Purpose Input Output (GPIO)
TODO
#### General Purpose Input Output Ports
TODO

#### General Purpose Input Output Peripherals
TODO
#### General Purpose Input Output Register 
TODO
### Register clock control (RCC)
TODO


## Protocols
### The Serial Wire Debug (SWD) protocol
Two wire protocol for accessing the ARM debug interface provided by the ITM.
The protocol uses 3 lines :
* SWDIO : A bi-directionnal line for data
* SWCLK : A clock line
* SWO   : An optionnal line used for output operation (like printf)
Using that protocol you can do a number of things, like program the flash
memory, access memory regions, use GDB

*Sources :*
[Great overview of the protocol](https://research.kudelskisecurity.com/2019/05/16/swd-arms-alternative-to-jtag/)
### The Serial Wide Viewer (SWV)

##Instructions
###Thumb 2

---

# Programming Languages
## C 
### The C Standart
TODO
### The Nano C Standart
TODO
### The build process
The build process is how you go from a human-readable code (like a .c file) to
code a processor is actually able to understand and execute. The build process
is divided in two sub process, the compilation and the linking stage

*Sources :*
[Lecture from Stefano
Zacchiroli](https://upsilon.cc/~zack/teaching/1112/ed6/cours-3.pdf) on the build process

#### The compilation stage
There are 5 steps main 
* **Preprocessing** : #include, #define, #undef and ifs (#ifdef, #ifndef) are
  resolved. The compiler generates the **.i file**
* **Parsing** : The parser goes through the code and looks for syntax errors and
  other errors report it to the programmer
* **Code generator and assembly code generation** : Here, the c code is transformed in assembly language and
  the **.s file** is generated
* **Producing object files and Assembler** : Here the assembly code is
  translated in an object file which is a binary version of the code that is
  not yet executable. The **.o file** is generated.

*Sources :*
* [Lecture from Stefano
Zacchiroli](https://upsilon.cc/~zack/teaching/1112/ed6/cours-3.pdf) on the **build process**
with some good information about the **pre-processing**
* [Article on compilers and
interpreter](https://medium.com/hackernoon/compilers-and-interpreters-3e354a2e41cf) with a nice overview of the **parsing**
step
* [How to go from intermediate code to
  assembly](https://cs.nyu.edu/courses/spring07/G22.3130-001/code_gen.html)
* [Article on the c compiler](http://courses.cms.caltech.edu/cs11/material/c/mike/misc/compiling_c.html) with relevant
section about the making of **the object file**

#### The linking stage
* **Linking object files** : In that stage, all the **.o** and the other librairies
  are merged together to create a single executable file.
* **Producing final executable** : A .elf file that is an executable file is
  created
* **Post processing of final executable** : Using tools like objcopy to create
  files like .bin and .exe files

*Sources :*
* [Beginner's Guide to Linkers](https://www.lurklurk.org/linkers/linkers.html#linker1)

### The C files
#### Elf files
### The assembler
### Embedded programming tip and tricks
#### Uses of binary operator 
##### Using the & operator to test a bit

Lets say I want to know if a number is odd or even. On way to do it could be to
see it in base 2 :

```
00010101 (21 in base 10)
```

when the least significant bit is 1, then I know it is odd. What I can do is
use what is called a mask wich represent the bit I want to check. In that case,
my mask is going to be 

```
00000001 
```

After that I'm going to do a bitwise &

```
00010101 (21 in base 10)
00000001 
--------
00000001
```

In c, it would be something like that

```c
( 21 & 1 ) == 1  ? printf("Its odd") : printf("Its even");
```

So I know it is in fact odd

##### Using the & operator to clear a bit

Lets say I want to set the third bit to 0 (clear it). What one might want to do
is to use the bitwise AND with a mask with 0 in the position you want to clear

Lets say I want to clear the 5th bit of 

##### Using the | operator to set a bit
##### Using the ^ operator to toggle a bit
## Relevant keywords 
### Extern
### Volatile
## Data alignment
---

#TODOS
## Stack
## Heap
## Embedded system
## Microprocessor
## Integrated Circuit (IC)
## OpenOCD
## GDB
## JTAG
## ST MCU
### Components
#### ST Link
## Schematics
### Solder Bridges (SB)

## Computer science
Here are some notions that are not MCU specifics that i felt might be notable
enough to note here 
### Data representation 
####Floating point standard (IEEE-754 standard)


## FIFO

# Acknowledgement
This file was written as a learning tool while I was following the [Microcontroller Embedded C : absolute begginner](https://www.udemy.com/course/microcontroller-embedded-c-programming/) from [FastBit Embedded Brain Academy](https://www.udemy.com/user/kiran-nayak-2/) on Udemy.
# Author 
eCarrier 
