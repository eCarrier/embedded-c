#include <stdio.h>

int main()
{ 
  char a1 = 'a';
  char a2 = 'p';
  char a3 = 'p';
  char a4 = 'l';
  char a5 = 'e';
  char a6 = 's';
  char a7 = '!';
  printf("Adress of a1 : %p\n", &a1);
  printf("Adress of a2 : %p\n", &a2);
  printf("Adress of a3 : %p\n", &a3);
  printf("Adress of a4 : %p\n", &a4);
  printf("Adress of a5 : %p\n", &a5);
  printf("Adress of a6 : %p\n", &a6);
  printf("Adress of a7 : %p\n", &a7);
  return 0;
}
