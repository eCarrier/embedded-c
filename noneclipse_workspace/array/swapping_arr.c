#include "../exercices_utilities.h"

int fill_array(long *array, size_t array_size);
void swap_arrays (long *const array_1, long * const array_2, const size_t array_1_size, const size_t array_2_size);
void print_array(const long * const array, const size_t array_size);

int main() 
{
  long array_1_size = 0, array_2_size = 0;
  if ( prompt_long("Enter the size of the first array",  &array_1_size) &&
    prompt_long("Enter the size of the second array",  &array_2_size) )
  {
    long array_1[array_1_size];
    long array_2[array_2_size];
    if (fill_array(array_1, array_1_size) && (fill_array(array_2, array_2_size)))
    {
      print_array(array_1, array_1_size);
      print_array(array_2, array_2_size);
      swap_arrays(array_1, array_2, array_1_size, array_2_size);
      print_array(array_1, array_1_size);
      print_array(array_2, array_2_size);
    }
    else 
      printf("Error while filling the array\n");
  }
  else
    printf("Error entering the size of arrays\n");
  return 0;
}
int fill_array(long *array, size_t array_size)
{
  int success = 1;
  for (size_t i = 0 ; success && i < array_size ; ++i)
  {
    printf("Enter element %lu of the array", i);
    success = prompt_long("", &array[i]);
  }
  return success ;
}
void swap_arrays (long * const array_1, long * const array_2, const size_t array_1_size, const size_t array_2_size)
{
  size_t max = array_1_size > array_2_size ? array_2_size : array_1_size ;
  long temp = 0;
  for ( size_t i = 0; i < max; ++i )
  {
    temp = array_1[i];
    array_1[i] = array_2[i];
    array_2[i] = temp;
  }
}
void print_array(const long * const array, const size_t array_size)
{
  for (size_t i = 0 ;  i < array_size ; ++i)
  {
    printf("%4ld\t", array[i]);
  }
  printf("\n");
}



