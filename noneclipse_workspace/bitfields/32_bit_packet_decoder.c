#include <stdio.h>
#include <stdint.h>

uint16_t bits_extractor(uint32_t val, uint8_t position, uint32_t count);
 
typedef struct 
{
  uint8_t crc;
  uint8_t status;
  uint16_t payload;
  uint8_t bat;
  uint8_t sensor;
  uint8_t longAddr;
  uint8_t shortAddr;
  uint8_t addrMode;
} Packet_t;

typedef struct
{
    uint32_t crc       :2;
    uint32_t status    :1;
    uint32_t payload   :12;
    uint32_t bat       :3;
    uint32_t sensor    :3;
    uint32_t longAddr  :8;
    uint32_t shortAddr :2;
    uint32_t addrMode  :1;
} Packet_t_bitfield;

int main()
{
  uint32_t user_input = 0;
  scanf("%x", &user_input);
  Packet_t p;
  Packet_t_bitfield p1;
  p.crc       = bits_extractor(user_input, 0, 2) ;
  p.status    = bits_extractor(user_input, 2, 1) ;
  p.payload   = bits_extractor(user_input, 3, 12);
  p.bat       = bits_extractor(user_input, 15, 3);
  p.sensor    = bits_extractor(user_input, 18, 3);
  p.longAddr  = bits_extractor(user_input, 21, 8);
  p.shortAddr = bits_extractor(user_input, 29, 2);
  p.addrMode  = bits_extractor(user_input, 31, 1);

  p1.crc       = bits_extractor(user_input, 0, 2) ;
  p1.status    = bits_extractor(user_input, 2, 1) ;
  p1.payload   = bits_extractor(user_input, 3, 12);
  p1.bat       = bits_extractor(user_input, 15, 3);
  p1.sensor    = bits_extractor(user_input, 18, 3);
  p1.longAddr  = bits_extractor(user_input, 21, 8);
  p1.shortAddr = bits_extractor(user_input, 29, 2);
  p1.addrMode  = bits_extractor(user_input, 31, 1);



  printf("%#x\n", p.crc);
  printf("%#x\n", p.status);
  printf("%#x\n", p.payload);
  printf("%#x\n", p.bat);
  printf("%#x\n", p.sensor);
  printf("%#x\n", p.longAddr);
  printf("%#x\n", p.shortAddr);
  printf("%#x\n", p.addrMode);
  printf("Sizeof : %lu\n", sizeof(p));

  printf("%#x\n", p1.crc);
  printf("%#x\n", p1.status);
  printf("%#x\n", p1.payload);
  printf("%#x\n", p1.bat);
  printf("%#x\n", p1.sensor);
  printf("%#x\n", p1.longAddr);
  printf("%#x\n", p1.shortAddr);
  printf("%#x\n", p1.addrMode);
  printf("Sizeof : %lu\n", sizeof(p1));


  return 0;
}

uint16_t bits_extractor(uint32_t val, uint8_t position, uint32_t count)
{
  uint32_t mask = 0;    
  for ( int i = 0; i < count; ++i )
  {
    mask |= 1 << i ;
  }
  printf("%X\n", mask);
  return ( ( val >> position ) & mask ) ;
}
