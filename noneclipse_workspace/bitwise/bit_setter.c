#include "../exercices_utilities.h"


int main()
{
  int user_input=0;
  if (prompt_int("Please enter an int that will have its 4th and 7th bit set to 1", &user_input)) 
  {
    printf("With the 4th and the 7th : %d\n", user_input | 72); 
  }
  wait_for_input();
  return 0;
}
