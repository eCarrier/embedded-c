#include "../exercices_utilities.h"

int main()
{
  float quoi;
  int val_to_test=0;
  if( prompt_int("Enter a number to test", &val_to_test) )
  {
    if ( (val_to_test & 1) == 1 )
      printf("It is odd\n");
    else
      printf("It is even\n");
  }
  else 
    printf("Error while parsing user input\n");
  wait_for_input();
  return 0;
}
