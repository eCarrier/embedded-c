#include <stdio.h>
#include <stdint.h>

int main()
{
  int32_t n1=0, n2=0;
  printf("Please enter 2 integers and this program will show you the larger of the two\n>");
  if(scanf("%d %d", &n1, &n2) == 2)
  { 
    if (n1 == n2)
      printf("They are equals\n");
    else 
      n1>n2 ? printf("%d is larger than %d\n", n1, n2) : printf("%d is larger than %d\n", n2, n1) ;
    printf("Press any key to exit\n");
    while (getchar() != '\n') ;
    getchar();
  }
  else 
    printf("Non integer was entered\n");
  return 0;
}

