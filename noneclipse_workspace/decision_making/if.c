#include <stdio.h>

const int AGE_MINIMUM = 18;
int main()
{
  int age=0;
  printf("**************************\n");
  printf("     Voting software\n");
  printf("**************************\n");
  printf("Please enter your age\n>");
  scanf("%d", &age);
  if (age < AGE_MINIMUM)
    printf("You are too young to vote chap\n");
  else
    printf("You are so old mate\n");
  printf("Press any character to exit\n");
  while (getchar() != '\n') {};
  getchar();
  return 0;
}
