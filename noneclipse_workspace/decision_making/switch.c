#include <stdio.h>
#include <stdint.h>

#define CODE_TRIANGLE   't'
#define CODE_TRAPEZOID  'z'
#define CODE_CIRCLE     'c'
#define CODE_SQUARE     's'
#define CODE_RECTANGLE  'r'

void wait_for_input();
int prompt_float(const char mess[], float *p_val);
int prompt_char(const char mess[], char *p_val);

int main()
{
  uint8_t prompt_ret=0;
  char user_selection=' ';
  float base_1=0, base_2=0, height=0, radius=0, side=0, width=0, length=0;
  printf("*****************************************\n");
  printf("        Shapes calculations\n");
  printf("*****************************************\n");
  printf("Press one of the following to start calculation\n");
  printf("Triangle : %c\n",   CODE_TRIANGLE);
  printf("Trapezoid : %c\n",  CODE_TRAPEZOID);
  printf("Circle  : %c\n",    CODE_CIRCLE);
  printf("Square : %c\n",     CODE_SQUARE);
  printf("Rectangle : %c\n",  CODE_RECTANGLE);
  prompt_char("Enter your selection:\n>", &user_selection);
  switch(user_selection)
  {
    case CODE_TRIANGLE:
      prompt_ret=prompt_float("Enter base :\n>", &base_1);
      if (base_1 <= 0)  
      {
        printf("Error : Negative or zero length is invalid");
        return 1;
      }
      prompt_ret=prompt_float("Enter height :\n>", &height);
      if (height <= 0) 
      {
        printf("Error : Negative or zero length is invalid");
        return 1;
      }
      break;
    case CODE_TRAPEZOID:
      prompt_ret=prompt_float("Enter base_1:\n>", &base_1);
      if (base_1 <= 0)
      {
        printf("Error : Negative or zero length is invalid");
        return 1;
      }
      prompt_ret=prompt_float("Enter base_2:\n>", &base_2);
      if (base_2 <= 0)
      {
        printf("Error : Negative or zero length is invalid");
        return 1;
      }
      prompt_ret=prompt_float("Enter height :\n>", &height);
      if (height <= 0)
      {
        printf("Error : Negative or zero length is invalid");
        return 1;
      }
      break;
    case CODE_CIRCLE:
      prompt_ret=prompt_float("Enter radius:\n>", &radius);
      if (radius <= 0)
      break;
    case CODE_SQUARE:
      prompt_ret=prompt_float("Enter side:\n>", &side);
      if (side <= 0)
      break;
    case CODE_RECTANGLE:
      prompt_ret=prompt_float("Enter width:\n>", &width);
      if (width <= 0) 
      prompt_ret=prompt_float("Enter length:\n>", &length);
      if (length <= 0)
      printf("Area is %0.4f", width*length);
      break;
  }
  return 0;
}

int prompt_float(const char mess[], float *p_val)
{
  printf("%s", mess); 
  return scanf("%f", p_val) > 0;
}

int prompt_char(const char mess[], char *p_val)
{
  printf("%s", mess); 
  return scanf("%c", p_val) > 0;
}

void wait_for_input(void)
{


}
