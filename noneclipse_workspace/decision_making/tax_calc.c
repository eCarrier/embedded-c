#include <stdio.h>
#include <stdint.h>

const float LEVEL_1_TAX_RATE = (float)0/100;
const float LEVEL_2_TAX_RATE = (float)12/100;
const float LEVEL_3_TAX_RATE = (float)32/100;
const float LEVEL_4_TAX_RATE = (float)32/100;

const int LEVEL_1_MAX_INCOME = 9525;
const int LEVEL_2_MIN_INCOME = 9526;
const int LEVEL_2_MAX_INCOME = 38700;
const int LEVEL_3_MIN_INCOME = 38701;
const int LEVEL_3_MAX_INCOME = 82500;
const int LEVEL_4_MIN_INCOME = 82500;

int main()
{
  int user_income=0;
  float taxes = 0;
  printf("**************************************\n");
  printf("       Tax income calculation\n");
  printf("**************************************\n");
  printf("Please enter your yearly income : \n>");
  if (scanf("%d", &user_income) == 1)
  {    
    printf("Entered income : %d\n", user_income);
    if (user_income <= LEVEL_1_MAX_INCOME) 
      taxes = user_income * (LEVEL_1_TAX_RATE);

    else if (user_income >= LEVEL_2_MIN_INCOME && user_income <= LEVEL_2_MAX_INCOME)
    {
      taxes =  user_income * (LEVEL_2_TAX_RATE);
    }

    else if (user_income >= LEVEL_3_MIN_INCOME && user_income <= LEVEL_3_MAX_INCOME)
      taxes = user_income * (LEVEL_3_TAX_RATE);

    else if (user_income > LEVEL_1_MAX_INCOME)
      taxes = (user_income * (LEVEL_4_TAX_RATE))+1000;

    printf("Taxes to pay :%0.2f\n", taxes);
  }
  else
    printf("Non number was entered\n");
  return 0;
}

