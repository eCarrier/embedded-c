#include <stdio.h>
#include <stdint.h>

#ifndef EXERCICES_UTILITIES_H
#define EXERCICES_UTILITIES_H


int prompt_float(const char mess[], float *p_val)
{
  printf("%s\n>", mess); 
  return scanf("%f", p_val) == 1;
}

int prompt_int(const char mess[], int *p_val)
{
  printf("%s\n>", mess); 
  return scanf("%d", p_val) == 1;
}

int prompt_long(const char mess[], long *p_val)
{
  printf("%s\n>", mess); 
  return scanf("%ld", p_val) == 1;
}

int prompt_char(const char mess[], char *p_val)
{
  printf("%s\n>", mess); 
  return scanf("%c", p_val) == 1;
}
  
void wait_for_input()
{
  printf("Enter any keys to exit\n>");
  while( getchar() != '\n' ) ;
  getchar();
}

#endif

