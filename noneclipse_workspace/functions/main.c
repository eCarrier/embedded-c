

#include <stdio.h>

int add_numbers(int, int, int);

int main()  
{   
  int first = 131;
  int second = 159;
  int third = 22;
  printf("Sum of %d, %d and %d : %d\n", first, second, third, add_numbers(first, second, third));
  return 0;
}

int add_numbers(int a, int b, int c)  
{
  return a+b+c;
}
