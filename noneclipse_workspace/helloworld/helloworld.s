	.file	"helloworld.c"
	.text
	.section	.rodata
.LC0:
	.string	"hello world"
.LC1:
	.string	"Today is a great day"
.LC2:
	.string	"f1() %d\n"
	.align 8
.LC3:
	.string	"David says ,  \" Programming is fun \""
.LC4:
	.string	"Good day!"
	.align 8
.LC5:
	.string	"David says, \"Programming is fun\""
	.align 8
.LC6:
	.string	"**Condition apply \"offers valid until tomorrow\""
.LC7:
	.string	"C:\\My vomputer\\my folder"
.LC8:
	.string	"D:/My documents/My file"
.LC9:
	.string	"\\\\\\ today is holiday \\\\\\"
	.align 8
.LC10:
	.string	"This is a triple quoted string \"\"\"this month has 30 days\"\"\""
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	leaq	.LC0(%rip), %rdi
	call	puts@PLT
	leaq	.LC1(%rip), %rdi
	call	puts@PLT
	movl	$0, %eax
	call	f1
	movl	%eax, %esi
	leaq	.LC2(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	leaq	.LC3(%rip), %rdi
	call	puts@PLT
	leaq	.LC4(%rip), %rdi
	call	puts@PLT
	leaq	.LC5(%rip), %rdi
	call	puts@PLT
	leaq	.LC6(%rip), %rdi
	call	puts@PLT
	leaq	.LC7(%rip), %rdi
	call	puts@PLT
	leaq	.LC8(%rip), %rdi
	call	puts@PLT
	leaq	.LC9(%rip), %rdi
	call	puts@PLT
	leaq	.LC10(%rip), %rdi
	call	puts@PLT
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.globl	f1
	.type	f1, @function
f1:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$10, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	f1, .-f1
	.ident	"GCC: (GNU) 10.1.0"
	.section	.note.GNU-stack,"",@progbits
