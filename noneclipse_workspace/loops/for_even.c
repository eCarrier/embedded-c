#include <stdio.h>
#include <stdint.h>

int main() 
{
  long lower_bond = 0, higher_bond = 0, i = 0;
  uint32_t even_count = 0;
  printf("Enter a range (2 numbers)\n>");
  if ( scanf("%ld %ld", &lower_bond, &higher_bond) == 2 )
  {
    if (lower_bond <= higher_bond)
    {
      for ( i = lower_bond ; i < higher_bond ; ++i)
      {
        if ( i % 2 == 0 )   
        {
          printf("%4ld\t", i) ;
          even_count++;
        }
      }
      printf("\n");
      printf("\nEven count %lu\n", even_count);
    }
    else
      printf("Error : Lower bond is bigger than higher bond\n");
  }
  else
  {
    printf("Error while reading values");
  }
  return 0;
}
