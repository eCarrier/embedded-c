#include <stdio.h>
#include <stdint.h>

int main()
{
  const uint8_t lower_bond = 1;
  int32_t height = 0;
  printf("Enter a non-zero positive number\n>");
  if ( ( scanf("%d", &height) == 1 ) && height > 0 )
  {
    for ( uint32_t i = lower_bond ; i <= height ; ++i )
    {
      for ( uint32_t y = i ; y >= lower_bond ; --y )
      {
        printf("%3u\t", y);
      }
      printf("\n");
    }
  }
  else 
    printf("Invalid input\n");
  return 0;
}
