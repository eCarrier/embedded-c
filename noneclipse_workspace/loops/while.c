#include <stdio.h>
#include <stdint.h>

int main()
{
  uint8_t i = 0;
  while ( i <= 10 )
  {
    printf("%d\n", i++);
  }
  return 0;
}
