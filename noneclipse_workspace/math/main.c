#include <stdio.h>
#include "math.h"

int main()
{
  long long n1 = 231;
  long long n2 = 150;
  printf("Operation add : n1=%d and n2=%d --> %d\n", n1, n2, math_add(n1,n2));
  printf("Operation sub : n1=%d and n2=%d --> %d\n", n1, n2, math_sub(n1,n2));
  printf("Operation div  : n1=%d and n2=%d --> %f\n", n1, n2, math_div(n1,n2));
  printf("Operation mult : n1=%d and n2=%d --> %d\n", n1, n2, math_mul(n1,n2));
  return 0;
}
