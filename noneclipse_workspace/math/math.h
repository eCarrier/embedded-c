#ifndef MATH_H
#define MATH_H

long long math_add(long long n1, long long n2);
long long math_sub(long long n1, long long n2);
long long math_mul(long long n1, long long n2);
float     math_div(long long n1, long long n2);

#endif
