#include <stdio.h>

int main ()
{
  char c1 = 100;
  char c2 = ' ';
  char *p_c1 = &c1;
  printf("value of c1 : %d\n",c1);
  printf("Address of c1 : %p\n",&c1);
  c2 = *p_c1;
  printf("Value stored at c2 : %d\n", c2);
  printf("Writing 65 at the address stored in p_c1\n");
  *p_c1=65;
  printf("Value at c1 : %d\n", c1);
  return 0;
}
