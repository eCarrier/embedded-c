#include <stdio.h>

int main()
{
  int a, b, c = 0;
  printf("Enter the first number\n> ");
  scanf("%d", &a);
  printf("Enter the second number\n> ");
  scanf("%d", &b);
  printf("Enter the third number\n> ");
  scanf("%d", &c);
  printf("Average of data input %f\n", (float)(a+b+c)/3);
  printf("Enter any character to exit\n");
  fflush(stdout);
  getchar();
}
