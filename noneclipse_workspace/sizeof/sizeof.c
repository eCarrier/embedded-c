#include <stdio.h>
#include <stdint.h>

int main()
{
  printf("Size of different datatypes . Char : %d\n",       sizeof(char)); 
  printf("Size of different datatypes . uint8_t : %d\n",    sizeof(uint8_t)); 
  printf("Size of different datatypes . uint64_t : %d\n",    sizeof(uint64_t)); 
  printf("Size of different datatypes . Short : %d\n",      sizeof(short)); 
  printf("Size of different datatypes . int : %d\n",        sizeof(int)); 
  printf("Size of different datatypes . long : %d\n",       sizeof(long)); 
  printf("Size of different datatypes . long long  : %d\n", sizeof(long long)); 
  return 0;
}
