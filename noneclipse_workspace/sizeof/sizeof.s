	.file	"sizeof.c"
	.text
	.section	.rodata
	.align 8
.LC0:
	.string	"Size of different datatypes . Char : %d\n"
	.align 8
.LC1:
	.string	"Size of different datatypes . uint8_t : %d\n"
	.align 8
.LC2:
	.string	"Size of different datatypes . uint64_t : %d\n"
	.align 8
.LC3:
	.string	"Size of different datatypes . Short : %d\n"
	.align 8
.LC4:
	.string	"Size of different datatypes . int : %d\n"
	.align 8
.LC5:
	.string	"Size of different datatypes . long : %d\n"
	.align 8
.LC6:
	.string	"Size of different datatypes . long long  : %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$1, %esi
	leaq	.LC0(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$8, %esi
	leaq	.LC2(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$2, %esi
	leaq	.LC3(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$4, %esi
	leaq	.LC4(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$8, %esi
	leaq	.LC5(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$8, %esi
	leaq	.LC6(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (GNU) 10.1.0"
	.section	.note.GNU-stack,"",@progbits
