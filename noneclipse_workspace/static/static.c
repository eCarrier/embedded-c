#include <stdio.h>

void otherfile_func2(void);
static void change_system_clock(int system_clock);

int func();

static int g_private_data;

int main()  
{ 
  for (int i = 0; i < 10; ++i)  
  { 
    printf("%d\n", func());
  }
  g_private_data = 100;
  printf("%d\n", g_private_data);
  otherfile_func2();
  printf("%d\n", g_private_data);
  return 0;
}

int func()
{
  static int var_1=0;
  var_1+=1;
  return var_1;
}

void change_system_clock(int system_clock)
{ 
  printf("System clock is now : %d\n", system_clock);
}
