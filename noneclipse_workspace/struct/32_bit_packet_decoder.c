#include <stdio.h>
#include <stdint.h>

uint8_t bits_extractor(uint32_t val, uint8_t position, uint32_t count);
 
typedef struct 
{
  uint8_t crc;
  uint8_t status;
  uint16_t payload;
  uint8_t bat;
  uint8_t sensor;
  uint8_t longAddr;
  uint8_t shortAddr;
  uint8_t addrMode;
} Packet_t;

int main()
{
  uint32_t user_input = 0;
  scanf("%x", &user_input);
  Packet_t p;
  p.crc       = bits_extractor(user_input, 0, 2) ;
  p.status    = bits_extractor(user_input, 2, 1) ;
  p.payload   = bits_extractor(user_input, 3, 12);
  p.bat       = bits_extractor(user_input, 15, 3);
  p.sensor    = bits_extractor(user_input, 18, 3);
  p.longAddr  = bits_extractor(user_input, 21, 8);
  p.shortAddr = bits_extractor(user_input, 29, 2);
  p.addrMode  = bits_extractor(user_input, 31, 1);

  printf("%#x\n", p.crc);
  printf("%#x\n", p.status);
  printf("%#x\n", p.payload);
  printf("%#x\n", p.bat);
  printf("%#x\n", p.sensor);
  printf("%#x\n", p.longAddr);
  printf("%#x\n", p.shortAddr);
  printf("%#x\n", p.addrMode);

  return 0;
}

uint8_t bits_extractor(uint32_t val, uint8_t position, uint32_t count)
{
  uint32_t mask = 0;    
  for ( int i = 0; i < count; ++i )
  {
    mask |= 1 << i ;
  }
  printf("%X\n", mask);
  return ( ( val >> position ) & mask ) ;
}
