#include <stdio.h>
#include <stdint.h>

struct CarModel 
{
  uint32_t  car_number;
  uint32_t  car_price;
  uint16_t  car_max_speed;
  float     car_weight;
};

void print_car(const struct CarModel *car_number);

int main()
{
  struct CarModel c1 = {1234, 20000, 220, 1330.02};
  struct CarModel c2 = {4567, 15000, 300, 2012.31};
  struct CarModel c3 = {.car_weight=100.02, .car_price=1414, .car_number=23, .car_max_speed=12314};
  print_car(&c1);
  print_car(&c2);
  print_car(&c3);
  printf("Sizeof CarModel : %lu\n", sizeof(struct CarModel));
  printf("Sizeof uint32_t : %lu\n", sizeof(uint32_t));
  printf("Sizeof float : %lu\n", sizeof(float));
  return 0;
}


void print_car(const struct CarModel *car)
{
  printf("Car : \n-> Car number \t:%u\n-> Car price \t:%u\n-> Car number \t:%u\n-> Car weight \t:%f\n", 
      car->car_number, car->car_price, car->car_max_speed, car->car_weight);
}

