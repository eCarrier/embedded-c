#include <stdio.h>
#include <stdint.h>

typedef struct 
{
  char  a;
  int   b;
  char  c;
  short d;

} DataSet_t;

int main()
{
  DataSet_t x;

  x.a = 0xC2;
  x.b = 0xEEFFFFFF;
  x.c = 0xFE;
  x.d = 0xABCD;

  printf("MEMORY MAP\n");
  printf("================================================\n");
  printf("Memory\t\tContent\n");
  printf("================================================\n");

  uint8_t *ptr;
  ptr = (uint8_t *) &x;
  
  uint32_t size_of_data = sizeof(DataSet_t);

  for (uint32_t i = 0; i < size_of_data; ++i)
  {
    printf("%p\t\t\t%X\n", ptr, *ptr);
    ptr++;
  }
  return 0;
}



/*
 *
 *
 *
 *  char  0000 0000 
 *
 *
 *
 * */
