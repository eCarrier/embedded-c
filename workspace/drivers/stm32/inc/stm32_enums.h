/*
 * stm32_enums.h
 *
 *  Created on: Aug 5, 2020
 *      Author: etienne
 */

#ifndef STM32_INC_STM32_ENUMS_H_
#define STM32_INC_STM32_ENUMS_H_

enum STATUS
{
	DISABLE = 0,
	ENABLE = 1
};

enum POWER_STATUS
{
	LOW = 0,
	HIGH = 1
};



#endif /* STM32_INC_STM32_ENUMS_H_ */
