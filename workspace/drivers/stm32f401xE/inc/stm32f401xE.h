/*
 * stm32401xE.h is the file containing various enum of the memory map
 * as shown in the mcu reference manual
 *
 *  Created on: Aug 3, 2020
 *      Author: etienne
 */

#include <stdint.h>
#include <stm32_enums.h>

#ifndef STM32F401XE_SRC_STM32401XE_H_
#define STM32F401XE_SRC_STM32401XE_H_

// Dumbass delay
#define DELAY() for (int i = 0; i < 500000 ; ++i);

//***********************************************************************************************
// 									Base addresses enums
//***********************************************************************************************
/**
 * Enum for memory base addresses
 */
enum MEMORY_BASE_ADDR
{
	FLASH_ADDR = 0x08000000, //!< FLASH_ADDR Location of the flash memory
	SRAM1_ADDR = 0x20000000, //!< SRAM1_ADDR Location of SRAM1
	SRAM = SRAM1_ADDR, //!< SRAM Location of SRAM
	SYSTEM_MEM_ADDR = 0x1FFF0000 //!< SYSTEM_MEM_ADDR Location of ROM
};
/**
 * Enum for the base addresses of base peripheral
 */
enum PERIPH_BASE_ADDR
{
	APB1_ADDR = 0x40000000, //!< APB1_ADDR Advanced peripheral bus 1 base address
	APB2_ADDR = 0x40010000, //!< APB2_ADDR Advanced peripheral bus 2 base address
	AHB1_ADDR = 0x40020000, //!< AHB1_ADDR Advanced high-speed bus 1 base address
	AHB2_ADDR = 0x50000000 //!< AHB2_ADDR Advanced high-speed bus 2 base address
};

/**
 * Enum for the memory map of APB1
 */
enum APB1_BASE_ADDR
{
	TIM2_ADDR = APB1_ADDR + 0x0000, //!< TIM2_ADDR General purpose timer 2 base address
	TIM3_ADDR = APB1_ADDR + 0x0400, //!< TIM3_ADDR General purpose timer 3 base address
	TIM4_ADDR = APB1_ADDR + 0x0800, //!< TIM4_ADDR General purpose timer 4 base address
	TIM5_ADDR = APB1_ADDR + 0x0C00, //!< TIM5_ADDR General purpose timer 5 base address RTC_BKP_ADDR  	= APB1_ADDR + 0x2800,//!< RTC_BKP_ADDR Real time clock base address
	WWDG_ADDR = APB1_ADDR + 0x2C00, //!< WWDG_ADDR Window watchdog base address
	IWDG_ADDR = APB1_ADDR + 0x3000, //!< IWDG_ADDR Independent watchdog base address
	I2S2ext_ADDR = APB1_ADDR + 0x3400, //!< I2S2ext_ADDR
	SPI2_ADDR = APB1_ADDR + 0x3800, //!< SPI2_ADDR Serial peripheral interface 2 base address
	SPI3_ADDR = APB1_ADDR + 0x3C00, //!< SPI3_ADDR	Serial peripheral interface 2 base address
	I2S3ext_ADDR = APB1_ADDR + 0x4000, //!< I2S3ext_ADDR
	USART2_ADDR = APB1_ADDR + 0x4400, //!< USART2_ADDR Universal synchronous asynchronous receiver trasnmitter 2 base address
	I2C1_ADDR = APB1_ADDR + 0x5400, //!< I2C1_ADDR Inter-integrated circuit interfgace 1 base address
	I2C2_ADDR = APB1_ADDR + 0x5800, //!< I2C2_ADDR Inter-integrated circuit interfgace 2 base address
	I2C3_ADDR = APB1_ADDR + 0x5C00, //!< I2C3_ADDR Inter-integrated circuit interfgace 3 base address
	PWR_ADDR = APB1_ADDR + 0x7000, //!< PWR_ADDR Power control base address
};

/**
 * Enum for the memory map of APB2
 */
enum APB2_BASE_ADDR
{
	TIM1_ADDR = APB2_ADDR + 0x0000, //!< TIM1_ADDR General-purpose timers
	USART1_ADDR = APB2_ADDR + 0x1000, //!< USART1_ADDR Universal synchronous asynchronous receiver trasnmitter 2 base address
	USART6_ADDR = APB2_ADDR + 0x1400, //!< USART6_ADDR Universal synchronous asynchronous receiver trasnmitter 2 base address
	ADC1_ADDR = APB2_ADDR + 0x2000, //!< ADC1_ADDR Analog-to-digital converter
	SDIO_ADDR = APB2_ADDR + 0x2C00, //!< SDIO_ADDR Secure digital input/output interface
	SPI1_ADDR = APB2_ADDR + 0x3000, //!< SPI1_ADDR Serial peripheral interface 1 base address
	SPI4_ADDR = APB2_ADDR + 0x3400, //!< SPI4_ADDR Serial peripheral interface 4 base address
	SYSCFG_ADDR = APB2_ADDR + 0x3800, //!< SYSCFG_ADDR System configuration controller base address
	EXTI_ADDR = APB2_ADDR + 0x3C00, //!< EXTI_ADDR
	TIM9_ADDR = APB2_ADDR + 0x4000, //!< TIM9_ADDR General purpose timer 9 base address
	TIM10_ADDR = APB2_ADDR + 0x4400, //!< TIM10_ADDR General purpose timer 10 base address
	TIM11_ADDR = APB2_ADDR + 0x4800 //!< TIM11_ADDR General purpose timer 11 base address
};

/**
 * Enum for the memory map of AHB1
 */
enum AHB1_BASE_ADDR
{
	GPIOA_ADDR = AHB1_ADDR + 0x0000, //!< GPIOA_ADDR General purpose input-output A base address
	GPIOB_ADDR = AHB1_ADDR + 0x0400, //!< GPIOB_ADDR General purpose input-output B base address
	GPIOC_ADDR = AHB1_ADDR + 0x0800, //!< GPIOC_ADDR General purpose input-output C base address
	GPIOD_ADDR = AHB1_ADDR + 0x0C00, //!< GPIOD_ADDR General purpose input-output D base address
	GPIOE_ADDR = AHB1_ADDR + 0x1000, //!< GPIOE_ADDR General purpose input-output E base address
	GPIOH_ADDR = AHB1_ADDR + 0x1C00, //!< GPIOH_ADDR General purpose input-output F base address
	CRC_ADDR = AHB1_ADDR + 0x3000, //!< CRC_ADDR Cyclic redundancy check calculation unit base address
	RCC_ADDR = AHB1_ADDR + 0x3800, //!< RCC_ADDR Reset and clock control base address
	FLASH_INT_ADDR = AHB1_ADDR + 0x3C00, //!< FLASH_INT_ADDR Flash interface register base address
	DMA1_ADDR = AHB1_ADDR + 0x6000, //!< DMA1_ADDR Direct memory access 1 base address
	DMA2_ADDR = AHB1_ADDR + 0x6400 //!< DMA2_ADDR Direct memory access 1 base address

};
/**
 * Enum for the memory map of AHB2
 */
enum AHB2_BASE_ADDR
{
	USB = AHB2_ADDR //!< USB USB on-the-go full-speed base address
};
//***********************************************************************************************
// 							Peripheral registers structures and enums
//***********************************************************************************************
// --------------------------------
// GPIO
// --------------------------------
/**
 * @struct Gpiox_reg_t
 * This is the generic GPIO registers structure
 *
 * @var Gpiox_reg_t::moder
 * The mode selection register
 *
 * @var Gpiox_reg_t::otyper
 * GPIO port output type register
 *
 * @var Gpiox_reg_t::ospeedr
 * GPIO port output speed register
 *
 * @var Gpiox_reg_t::pupdr
 * GPIO port pull-up/pull-down register
 *
 * @var Gpiox_reg_t::idr
 * GPIO port input data register
 *
 * @var Gpiox_reg_t::odr
 * GPIO port input data register
 *
 * @var Gpiox_reg_t::bsrr
 * GPIO port bit set/reset register
 *
 * @var Gpiox_reg_t::lckr
 * GPIO port bit set/reset register
 *
 * @var Gpiox_reg_t::afr
 * GPIO alternate function low register
 */
typedef struct
{
	uint32_t volatile moder;
	uint32_t volatile otyper;
	uint32_t volatile ospeedr;
	uint32_t volatile pupdr;
	uint32_t volatile idr;
	uint32_t volatile odr;
	uint32_t volatile bsrr;
	uint32_t volatile lckr;
	uint32_t volatile afr[2];
} Gpiox_reg_t;

/**
 * Enum for GPIO port mode options
 */
enum GPIO_PORT_MODE
{
	GPIO_PM_INPUT,    //!< INPUT
	GPIO_PM_GPOM,     //!< GPOM General purpose output mode
	GPIO_PM_ALTERNATE,     //!< ALTERNATE
	GPIO_PM_ANALOG    //!< ANALOG
};

/**
 * Enum for GPIO output speed register
 */
enum GPIO_OSPEED
{
	GPIO_SPEED_LOW,   //!< LOW
	GPIO_SPEED_MEDIUM,   //!< MEDIUM
	GPIO_SPEED_HIGH,  //!< HIGH
	GPIO_SPEED_VHIGH //!< V_HIGH
};
/**
 *  Enum for the GPIO pull up/pull-down resistor mode
 */
enum GPIO_PUPD
{
	GPIO_PUPD_NONE,     //!< NO_PUPD No pull-up/pull-down
	GPIO_PUPD_PULLUP,      //!< PULLUP Pull-up ownly
	GPIO_PUPD_PULLDOWN,    //!< PULLDOWN Pull-down
	GPIO_PUPD_RESERVED    //!< PUPD_RESERVED Reserved
};
/**
 *  Enum for the output mode type
 */
enum GPIO_OTYPE
{
	GPIO_OUTPUT_PUSH_PULL,    //!< PUSH_PULL
	GPIO_OUTPUT_OPEN_DRAIN    //!< OPEN_DRAIN
};
enum GPIO_ALT_MODE
{
	GPIO_ALT_MODE_AF0,
	GPIO_ALT_MODE_AF1,
	GPIO_ALT_MODE_AF2,
	GPIO_ALT_MODE_AF3,
	GPIO_ALT_MODE_AF4,
	GPIO_ALT_MODE_AF5,
	GPIO_ALT_MODE_AF6,
	GPIO_ALT_MODE_AF7,
	GPIO_ALT_MODE_AF8,
	GPIO_ALT_MODE_AF9,
	GPIO_ALT_MODE_AF10,
	GPIO_ALT_MODE_AF11,
	GPIO_ALT_MODE_AF12,
	GPIO_ALT_MODE_AF13,
	GPIO_ALT_MODE_AF14,
	GPIO_ALT_MODE_AF15
};

enum GPIO_PIN_NUMBER
{
	GPIO_PIN_0 = 0,
	GPIO_PIN_1,
	GPIO_PIN_2,
	GPIO_PIN_3,
	GPIO_PIN_4,
	GPIO_PIN_5,
	GPIO_PIN_6,
	GPIO_PIN_7,
	GPIO_PIN_8,
	GPIO_PIN_9,
	GPIO_PIN_10,
	GPIO_PIN_11,
	GPIO_PIN_12,
	GPIO_PIN_13,
	GPIO_PIN_14,
	GPIO_PIN_15
};

// --------------------------------
// RCC
// --------------------------------
/**
 * @struct Rcc_reg_t
 * This is the reset and clock control register map structure
 *
 * @var cr
 * Clock control register
 *
 * @var pllcfgr
 * Pll configuration register
 *
 * @var cfgr
 * clock configuration register
 *
 * @var cir
 * clock interrupt register
 *
 * @var ahb1rstr
 * AHB1 peripheral reset register
 *
 * @var ahb2rstr
 * AHB2 peripheral reset register
 *
 * @var reserved_1
 * @var reserved_2
 *
 * @var apb1rstr
 * APB1 peripheral reset register
 *
 * @var apb2rstr
 * PB2 peripheral reset register
 *
 * @var reserved_3
 * @var reserved_4
 *
 * @var ahb1enr
 * AHB1 peripheral clock enable register
 *
 * @var ahb2enr
 * AHB2 peripheral clock enable register
 *
 * @var reserved_5
 * @var reserved_6
 *
 * @var apb1enr
 * APB1 peripheral clock enable registe
 *
 * @var apb2enr
 * APB2 peripheral clock enable register
 *
 * @var reserved_7
 * @var reserved_8
 *
 * @var ahb1lpenr
 * AHB1 peripheral clock enable in low power mode register
 *
 * @var ahb2lpenr
 * AHB2 peripheral clock enable in low power mode register
 *
 * @var reserved_9
 * @var reserved_10
 *
 * @var bdcr
 * Backup domain control registe
 *
 * @var csr
 * Clock control & status register
 *
 * @var reserved_11
 * @var reserved_12
 *
 * @var sscgr
 * spread spectrum clock generation registe
 *
 * @var plli2scfgr
 * PLLI2S configuration register
 *
 */
typedef struct
{
	uint32_t volatile cr;
	uint32_t volatile pllcfgr;
	uint32_t volatile cfgr;
	uint32_t volatile cir;
	uint32_t volatile ahb1rstr;
	uint32_t volatile ahb2rstr;
	const uint32_t reserved_1;
	const uint32_t reserved_2;
	uint32_t volatile apb1rstr;
	uint32_t volatile apb2rstr;
	const uint32_t reserved_3;
	const uint32_t reserved_4;
	uint32_t volatile ahb1enr;
	uint32_t volatile ahb2enr;
	const uint32_t reserved_5;
	const uint32_t reserved_6;
	uint32_t volatile apb1enr;
	uint32_t volatile apb2enr;
	const uint32_t reserved_7;
	const uint32_t reserved_8;
	uint32_t volatile ahb1lpenr;
	uint32_t volatile ahb2lpenr;
	const uint32_t reserved_9;
	const uint32_t reserved_10;
	uint32_t volatile bdcr;
	uint32_t volatile csr;
	const uint32_t reserved_11;
	const uint32_t reserved_12;
	uint32_t volatile sscgr;
	uint32_t volatile plli2scfgr;
	uint32_t volatile dckcfgr;
} Rcc_reg_t;

//***********************************************************************************************
// 					  	Global handlers declaration for various registers
//***********************************************************************************************

// RCC
extern Rcc_reg_t *const RCC; //= (Rcc_reg_t *)RCC_ADDR;
// Different GPIO registers
extern Gpiox_reg_t *const GPIOA; // = (Gpiox_reg_t *)GPIOA_ADDR;
extern Gpiox_reg_t *const GPIOB; // = (Gpiox_reg_t *)GPIOB_ADDR;
extern Gpiox_reg_t *const GPIOC; // = (Gpiox_reg_t *)GPIOC_ADDR;
extern Gpiox_reg_t *const GPIOD; // = (Gpiox_reg_t *)GPIOD_ADDR;
extern Gpiox_reg_t *const GPIOE; // = (Gpiox_reg_t *)GPIOE_ADDR;
extern Gpiox_reg_t *const GPIOH; // = (Gpiox_reg_t *)GPIOH_ADDR;

//***********************************************************************************************
// 					  				Functions
//***********************************************************************************************
//-------------------------------------------
// Enable functions for various peripherals
//-------------------------------------------
// GPIO
/**
 * Enable GPIOA peripheral clock
 */
void gpioa_pclk_en( void ); //{ RCC->ahb1enr |= (1 << 0); };
/**
 * Enable GPIOB peripheral clock
 */
void gpiob_pclk_en( void ); // {RCC->ahb1enr |= (1 << 1); };
/**
 * Enable GPIOC peripheral clock
 */
void gpioc_pclk_en( void ); // {RCC->ahb1enr |= (1 << 2); };
/**
 * Enable GPIOD peripheral clock
 */
void gpiod_pclk_en( void ); // {RCC->ahb1enr |= (1 << 3); }
/**
 * Enable GPIOE peripheral clock
 */
void gpioe_pclk_en( void ); // {RCC->ahb1enr |= (1 << 4); }
/**
 * Enable GPIOH peripheral clock
 */
void gpioh_pclk_en( void ); // {RCC->ahb1enr |= (1 << 5); }
// I2Cx
/**
 * Enable I2C1 peripheral clock
 */
void i2c1_pclk_en( void ); // {RCC->apb1enr |= (1 << 21); }
/**
 * Enable I2C2 peripheral clock
 */
void i2c2_pclk_en( void ); // {RCC->ahb1enr |= (1 << 22); }
/**
 * Enable I2C3 peripheral clock
 */
void i2c3_pclk_en( void ); // {RCC->ahb1enr |= (1 << 23); }
// SPIx
/**
 * Enable SPI1 peripheral clock
 */
void spi1_pclk_en( void ); // {RCC->apb2enr |= (1 << 12); }
/**
 * Enable SPI2 peripheral clock
 */
void spi2_pclk_en( void ); // {RCC->apb1enr |= (1 << 14); }
/**
 * Enable SPI3 peripheral clock
 */
void spi3_pclk_en( void ); // {RCC->apb1enr |= (1 << 15); }
/**
 * Enable SPI4 peripheral clock
 */
void spi4_pclk_en( void ); // {RCC->apb2enr |= (1 << 13); }
// USARTx
/**
 * Enable USART1 peripheral clock
 */
void usart1_pclk_en( void ); // {RCC->apb2enr |= (1 << 4); }
/**
 * Enable USART2 peripheral clock
 */
void usart2_pclk_en( void ); // {RCC->apb1enr |= (1 << 17); }
/**
 * Enable USART6 peripheral clock
 */
void usart6_pclk_en( void ); // {RCC->apb2enr |= (1 << 5); }
// SYSCFG
/**
 * Enable SYSCFG peripheral clock
 */
void syscfg_pclk_en( void ); // {RCC->apb2enr |= (1 << 14); }

//-------------------------------------------
// disable functions for various peripherals
//-------------------------------------------

// GPIO
/**
 * Disable GPIOA peripheral clock
 */
void gpioa_pclk_di( void ); // {RCC->ahb1enr &= ~(1 << 0); }
/**
 * Disable GPIOB peripheral clock
 */
void gpiob_pclk_di( void ); // {RCC->ahb1enr &= ~(1 << 1); }
/**
 * Disable GPIOC peripheral clock
 */
void gpioc_pclk_di( void ); // {RCC->ahb1enr &= ~(1 << 2); }
/**
 * Disable GPIOD peripheral clock
 */
void gpiod_pclk_di( void ); // {RCC->ahb1enr  &= ~(1 << 3); }
/**
 * Disable GPIOE peripheral clock
 */
void gpioe_pclk_di( void ); // {RCC->ahb1enr &= ~(1 << 4); }
/**
 * Disable GPIOH peripheral clock
 */
void gpioh_pclk_di( void ); // {RCC->ahb1enr &= ~(1 << 5); }
// I2Cx
/**
 * Disable I2C1 peripheral clock
 */
void i2c1_pclk_di( void ); // {RCC->apb1enr &= ~(1 << 21); }
/**
 * Disable I2C2 peripheral clock
 */
void i2c2_pclk_di( void ); // {RCC->ahb1enr &= ~(1 << 22); }
/**
 * Disable I2C3 peripheral clock
 */
void i2c3_pclk_di( void ); // {RCC->ahb1enr &= ~(1 << 23); }
// SPIx
/**
 * Disable SPI1 peripheral clock
 */
void spi1_pclk_di( void ); // {RCC->apb2enr &= ~(1 << 12); }
/**
 * Disable SPI2 peripheral clock
 */
void spi2_pclk_di( void ); // {RCC->apb1enr &= ~(1 << 14); }
/**
 * Disable SPI3 peripheral clock
 */
void spi3_pclk_di( void ); // {RCC->apb1enr &= ~(1 << 15); }
/**
 * Disable SPI4 peripheral clock
 */
void spi4_pclk_di( void ); // {RCC->apb2enr &= ~(1 << 13); }
// USARTx
/**
 * Disable USART1 peripheral clock
 */
void usart1_pclk_di( void ); // {RCC->apb2enr &= ~(1 << 4); }
/**
 * Disable USART2 peripheral clock
 */
void usart2_pclk_di( void ); // {RCC->apb1enr &= ~(1 << 17); }
/**
 * Disable USART6 peripheral clock
 */
void usart6_pclk_di( void ); // {RCC->apb2enr &= ~(1 << 5); }
// SYSCFG
/**
 * Disable SYSCFG peripheral clock
 */
void syscfg_pclk_di( void ); // {RCC->apb2enr &= ~(1 << 14); }

//-------------------------------------------
// resets functions for various peripherals
//-------------------------------------------

// GPIO

/**
 * Resets GPIOA
 */
void gpioa_reset(void);
/**
 * Resets GPIOB
 */
void gpiob_reset(void);

/**
 * Resets GPIOC
 */
void gpioc_reset(void);

/**
 * Resets GPIOD
 */
void gpiod_reset(void);

/**
 * Resets GPIOE
 */
void gpioe_reset(void);

/**
 * Resets GPIOH
 */
void gpioh_reset(void);



#endif /* STM32F401XE_SRC_STM32401XE_H_ */
