/*
 * stm32f401xE_gpio_driver.h
 *
 *  Created on: Aug 4, 2020
 *      Author: etienne
 */
#include "stm32f401xE.h"

#ifndef STM32F401XE_INC_STM32F401XE_GPIO_DRIVER_H_
#define STM32F401XE_INC_STM32F401XE_GPIO_DRIVER_H_

//************************************************************************
// 						Structure definitions
//************************************************************************

/**
 *	@struct Gpiox_pin_config
 *	Structure containing various configuration settings for a generic GPIO pin
 *
 *	@var pin_number
 *	the pin number
 *
 *	@var pin_mode
 *	The current pin mode
 *
 *	@var pin_speed
 *	The current pin speed
 *
 *	@var pin_pupd
 *	The pin's pull-up/pull-down resistor settings
 *
 *	@var pin_output_type
 *	The pin's current output type
 *
 *	@var pin_alt_mode
 *	The pin's alternative mode
 */
typedef struct
{
	enum GPIO_PIN_NUMBER pin_number;
	enum GPIO_PORT_MODE pin_mode;
	enum GPIO_OSPEED pin_speed;
	enum GPIO_PUPD pin_pupd;
	enum GPIO_OTYPE pin_output_type;
	enum GPIO_ALT_MODE pin_alt_mode;
} Gpiox_pin_config_t;

/**
 * @struct Gpiox_handle_t
 *
 * Structure for general GPIO operations
 *
 * @var p_gpiox
 * Pointer to the base register
 *
 * @var gpio_pin_config
 * Holds the GPIO pin config
 *
 */

typedef struct
{
	Gpiox_reg_t *p_gpiox;
	Gpiox_pin_config_t pin_config;
} Gpiox_handle_t;

//****************************************************************************
//  					   		API
//****************************************************************************

// Enabling the GPIO pin in the RCC
/**
 *
 * Enables a GPIO port
 *
 * @param p_gpiox is the GPIOx register structure pointer
 * @param enable is either ENABLE or DISABLE
 *
 * @return 1 when the clock control was successfully enabled, else 0
 */
uint8_t gpio_clock_control( Gpiox_reg_t *p_gpiox, enum STATUS enable );

// Initialisation and reset the GPIO with a given configuration
/**
 *
 * Initialise a GPIO peripheral with given configuration
 *
 * @param handle is the GPIO handle that contains the configuration
 * @return 1 when it successfully was initiated, else 0
 */
void gpio_init( Gpiox_handle_t *handle );

/**
 *
 * Resets a gpio peripheral
 *
 * @param p_gpio The GPIO register structure to reset
 */
void gpio_reset( Gpiox_reg_t *const p_gpio );

// Read/write functionnality

/**
 * Reads a pin on a GPIO port
 *
 * @param p_gpio
 * @param pin_number The pin number to read
 * @return Pin's value
 */
uint8_t gpio_read_input_pin( Gpiox_reg_t *p_gpio, const uint8_t pin_number );
/**
 * Reads the gpio input port
 *
 * @param p_gpio
 * @return Pin's value
 */
uint16_t gpio_read_input_port( Gpiox_reg_t *const p_gpio );

/**
 * Writes to a GPIO output port pin
 *
 * @param p_gpio The GPIO port
 * @param pin_number The pin number
 * @param value The value to write
 */
void gpio_write_output_pin( Gpiox_reg_t *const p_gpio, uint8_t const pin_number,
		uint8_t const value );
/**
 * Writes to a GPIO output port
 *
 * @param p_gpio
 * @param value
 */
void gpio_write_output_port( Gpiox_reg_t *const p_gpio, uint16_t const value );
/**
 * Toggle GPIO pin
 * @param p_gpio The GPIO port
 * @param pin_number The pin number
 */
void gpio_toggle_output_pin(  Gpiox_reg_t *const p_gpio, uint8_t const pin_number );

// Interrupt management
void gpio_irq_config( void );
void gpio_irq_handling( void );

//****************************************************************************
// 							Utility
//****************************************************************************
/**
 * Validate a GPIO handler values
 *
 * @param handle the handle to validate
 * @return 1 when the handler is valid, 0 else
 */
uint8_t validate_gpio_handler( Gpiox_handle_t *handle );

/**
 * "Factory" to build a pin config
 *
 * Used to prevent bloat from having to init all the struct and focuses on
 * the most important stuff ( aka pin number and pin mode )
 *
 * @param pin_number the pin number
 * @param pin_mode the pin mode
 * @return a pin configuration
 */
Gpiox_pin_config_t gpio_build_pin_config(enum GPIO_PIN_NUMBER pin_number, enum GPIO_PORT_MODE pin_mode);

#endif /* STM32F401XE_INC_STM32F401XE_GPIO_DRIVER_H_ */
