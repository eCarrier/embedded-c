/*
 * stm32f401xE.c
 *
 *  Created on: Aug 5, 2020
 *      Author: etienne
 */
#define INLINE
#include "stm32f401xE.h"

// RCC
Rcc_reg_t *const RCC = ( Rcc_reg_t* ) RCC_ADDR;
// Different GPIO registers
Gpiox_reg_t *const GPIOA = ( Gpiox_reg_t* ) GPIOA_ADDR;
Gpiox_reg_t *const GPIOB = ( Gpiox_reg_t* ) GPIOB_ADDR;
Gpiox_reg_t *const GPIOC = ( Gpiox_reg_t* ) GPIOC_ADDR;
Gpiox_reg_t *const GPIOD = ( Gpiox_reg_t* ) GPIOD_ADDR;
Gpiox_reg_t *const GPIOE = ( Gpiox_reg_t* ) GPIOE_ADDR;
Gpiox_reg_t *const GPIOH = ( Gpiox_reg_t* ) GPIOH_ADDR;

//***********************************************************************************************
// 					  				Functions
//***********************************************************************************************
//-------------------------------------------
// Enable functions for various peripherals
//-------------------------------------------
// GPIO
/**
 * Enable GPIOA peripheral clock
 */
void gpioa_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 0 );
}
;
/**
 * Enable GPIOB peripheral clock
 */
void gpiob_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 1 );
}
;
/**
 * Enable GPIOC peripheral clock
 */
void gpioc_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 2 );
}
;
/**
 * Enable GPIOD peripheral clock
 */
void gpiod_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 3 );
}
/**
 * Enable GPIOE peripheral clock
 */
void gpioe_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 4 );
}
/**
 * Enable GPIOH peripheral clock
 */
void gpioh_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 5 );
}
// I2Cx
/**
 * Enable I2C1 peripheral clock
 */
void i2c1_pclk_en( void )
{
	RCC->apb1enr |= ( 1 << 21 );
}
/**
 * Enable I2C2 peripheral clock
 */
void i2c2_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 22 );
}
/**
 * Enable I2C3 peripheral clock
 */
void i2c3_pclk_en( void )
{
	RCC->ahb1enr |= ( 1 << 23 );
}
// SPIx
/**
 * Enable SPI1 peripheral clock
 */
void spi1_pclk_en( void )
{
	RCC->apb2enr |= ( 1 << 12 );
}
/**
 * Enable SPI2 peripheral clock
 */
void spi2_pclk_en( void )
{
	RCC->apb1enr |= ( 1 << 14 );
}
/**
 * Enable SPI3 peripheral clock
 */
void spi3_pclk_en( void )
{
	RCC->apb1enr |= ( 1 << 15 );
}
/**
 * Enable SPI4 peripheral clock
 */
void spi4_pclk_en( void )
{
	RCC->apb2enr |= ( 1 << 13 );
}
// USARTx
/**
 * Enable USART1 peripheral clock
 */
void usart1_pclk_en( void )
{
	RCC->apb2enr |= ( 1 << 4 );
}
/**
 * Enable USART2 peripheral clock
 */
void usart2_pclk_en( void )
{
	RCC->apb1enr |= ( 1 << 17 );
}
/**
 * Enable USART6 peripheral clock
 */
void usart6_pclk_en( void )
{
	RCC->apb2enr |= ( 1 << 5 );
}
// SYSCFG
/**
 * Enable SYSCFG peripheral clock
 */
void syscfg_pclk_en( void )
{
	RCC->apb2enr |= ( 1 << 14 );
}

//-------------------------------------------
// disable functions for various peripherals
//-------------------------------------------

// GPIO
/**
 * Disable GPIOA peripheral clock
 */
void gpioa_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 0 );
}
/**
 * Disable GPIOB peripheral clock
 */
void gpiob_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 1 );
}
/**
 * Disable GPIOC peripheral clock
 */
void gpioc_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 2 );
}
/**
 * Disable GPIOD peripheral clock
 */
void gpiod_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 3 );
}
/**
 * Disable GPIOE peripheral clock
 */
void gpioe_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 4 );
}
/**
 * Disable GPIOH peripheral clock
 */
void gpioh_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 5 );
}
// I2Cx
/**
 * Disable I2C1 peripheral clock
 */
void i2c1_pclk_di( void )
{
	RCC->apb1enr &= ~ ( 1 << 21 );
}
/**
 * Disable I2C2 peripheral clock
 */
void i2c2_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 22 );
}
/**
 * Disable I2C3 peripheral clock
 */
void i2c3_pclk_di( void )
{
	RCC->ahb1enr &= ~ ( 1 << 23 );
}
// SPIx
/**
 * Disable SPI1 peripheral clock
 */
void spi1_pclk_di( void )
{
	RCC->apb2enr &= ~ ( 1 << 12 );
}
/**
 * Disable SPI2 peripheral clock
 */
void spi2_pclk_di( void )
{
	RCC->apb1enr &= ~ ( 1 << 14 );
}
/**
 * Disable SPI3 peripheral clock
 */
void spi3_pclk_di( void )
{
	RCC->apb1enr &= ~ ( 1 << 15 );
}
/**
 * Disable SPI4 peripheral clock
 */
void spi4_pclk_di( void )
{
	RCC->apb2enr &= ~ ( 1 << 13 );
}
// USARTx
/**
 * Disable USART1 peripheral clock
 */
void usart1_pclk_di( void )
{
	RCC->apb2enr &= ~ ( 1 << 4 );
}
/**
 * Disable USART2 peripheral clock
 */
void usart2_pclk_di( void )
{
	RCC->apb1enr &= ~ ( 1 << 17 );
}
/**
 * Disable USART6 peripheral clock
 */
void usart6_pclk_di( void )
{
	RCC->apb2enr &= ~ ( 1 << 5 );
}
// SYSCFG
/**
 * Disable SYSCFG peripheral clock
 */
void syscfg_pclk_di( void )
{
	RCC->apb2enr &= ~ ( 1 << 14 );
}

//-------------------------------------------
// resets functions for various peripherals
//-------------------------------------------

// GPIO

/**
 * Resets GPIOA
 */
void gpioa_reset( void )
{
	RCC->ahb1rstr |= ( 1 << 0 );
	RCC->ahb1rstr &= ~ ( 1 << 0 );
}
/**
 * Resets GPIOB
 */
void gpiob_reset( void )
{
	RCC->ahb1rstr |= ( 1 << 1 );
	RCC->ahb1rstr &= ~ ( 1 << 1 );

}
/**
 * Resets GPIOC
 */
void gpioc_reset( void )
{
	RCC->ahb1rstr |= ( 1 << 2 );
	RCC->ahb1rstr &= ~ ( 1 << 2 );
}
/**
 * Resets GPIOD
 */
void gpiod_reset( void )
{
	RCC->ahb1rstr |= ( 1 << 3 );
	RCC->ahb1rstr &= ~ ( 1 << 3 );
}
/**
 * Resets GPIOE
 */
void gpioe_reset( void )
{
	RCC->ahb1rstr |= ( 1 << 4 );
	RCC->ahb1rstr &= ~ ( 1 << 4 );
}
/**
 * Resets GPIOH
 */
void gpioh_reset( void )
{
	RCC->ahb1rstr |= ( 1 << 7 );
	RCC->ahb1rstr &= ~ ( 1 << 7 );

}
