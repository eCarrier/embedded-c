/*
 * stm32f401xE_gpio_driver.c
 *
 *  Created on: Aug 4, 2020
 *      Author: etienne
 */

#include "stm32f401xE_gpio_driver.h"
#include "stdlib.h"

/**
 *
 * Enables a GPIO port
 *
 * @param p_gpiox is the GPIOx register structure pointer
 * @param enable is either ENABLE or DISABLE
 *
 * @return 1 when the clock control was successfully enabled, else 0
 */
uint8_t gpio_clock_control( Gpiox_reg_t *const p_gpiox,
		const enum STATUS enable )
{
	uint8_t success = 0;
	// Prevent null ptr
	if ( p_gpiox != NULL )
	{
		// Test every GPIO peripheral
		if ( p_gpiox == GPIOA )
		{
			if ( enable )
				gpioa_pclk_en();
			else
				gpioa_pclk_di();
			success = 1;
		}
		else if ( p_gpiox == GPIOB )
		{
			if ( enable )
				gpiob_pclk_en();
			else
				gpiob_pclk_di();
			success = 1;
		}
		else if ( p_gpiox == GPIOC )
		{
			if ( enable )
				gpioc_pclk_en();
			else
				gpioc_pclk_di();
			success = 1;
		}
		else if ( p_gpiox == GPIOD )
		{
			if ( enable )
				gpiod_pclk_en();
			else
				gpiod_pclk_di();
			success = 1;
		}
		else if ( p_gpiox == GPIOE )
		{
			if ( enable )
				gpioe_pclk_en();
			else
				gpioe_pclk_di();
			success = 1;
		}
		else if ( p_gpiox == GPIOE )
		{
			if ( enable )
				gpioh_pclk_en();
			else
				gpioh_pclk_di();
			success = 1;
		}
	}
	return success;
}

// Initialisation and reset the GPIO with a given configuration

/**
 *
 * Initialise a GPIO peripheral with given configuration
 *
 * @param handle is the GPIO handle that contains the configuration
 * @return 1 when it successfully was initiated, else 0
 */
void gpio_init( Gpiox_handle_t *const handle )
{
	uint8_t two_bits_pin_number = 2 * ( handle->pin_config.pin_number );

	// Preventing null ptr
	if ( handle != NULL )
	{
		if ( validate_gpio_handler( handle ) )
		{
			// Clearing all register that have two bits
			handle->p_gpiox->moder &= ~ ( 3 << two_bits_pin_number );
			handle->p_gpiox->ospeedr &= ~ ( 3 << two_bits_pin_number );
			handle->p_gpiox->pupdr &= ~ ( 3 << two_bits_pin_number );

			// Pin mode
			// TODO : handling interrupt mode
			handle->p_gpiox->moder |= handle->pin_config.pin_mode
					<< two_bits_pin_number;
			// Pin speed
			handle->p_gpiox->ospeedr |= handle->pin_config.pin_speed
					<< two_bits_pin_number;
			// Pin PUPD
			handle->p_gpiox->pupdr |= handle->pin_config.pin_pupd << two_bits_pin_number;
			// Pin output type
			handle->p_gpiox->otyper |= handle->pin_config.pin_output_type
					<< handle->pin_config.pin_number;
			// Pin alternative mode
			if ( handle->pin_config.pin_mode == GPIO_PM_ALTERNATE )
			{
				// Find if we have afr[0] or afr[1]
				uint8_t alt_reg_selection = handle->pin_config.pin_number / 8;
				// Find the offset
				uint8_t pin_offset = 4 * ( handle->pin_config.pin_number % 8 );

				handle->p_gpiox->afr[alt_reg_selection] |=
						handle->pin_config.pin_alt_mode << pin_offset;
			}
		}
	}
}


/**
 *
 * Resets a gpio peripheral
 *
 * @param p_gpio The GPIO register structure to reset
 */
void gpio_reset( Gpiox_reg_t *const p_gpiox )
{
	// Prevent null pointer
	if ( p_gpiox != NULL )
	{
		// Test every GPIO peripheral
		if ( p_gpiox == GPIOA )
		{
			gpioa_reset();
		}
		else if ( p_gpiox == GPIOB )
		{
			gpiob_reset();
		}
		else if ( p_gpiox == GPIOC )
		{
			gpioc_reset();
		}
		else if ( p_gpiox == GPIOD )
		{
			gpiod_reset();
		}
		else if ( p_gpiox == GPIOE )
		{
			gpioe_reset();
		}
		else if ( p_gpiox == GPIOE )
		{
			gpioh_reset();
		}
	}

}

// Read/write functionnality
/**
 * Reads a pin on a GPIO port
 *
 * @param p_gpio
 * @param pin_number The pin number to read
 * @return Pin's value (0 or 1)
 */
uint8_t gpio_read_input_pin(Gpiox_reg_t *p_gpio, const uint8_t pin_number)
{
	return (p_gpio->idr >> pin_number) & 1;
}

/**
 * Reads the gpio input port
 *
 * @param p_gpio
 * @return Whole port
 */
uint16_t gpio_read_input_port(Gpiox_reg_t *p_gpio)
{
	return (uint16_t)(p_gpio->idr);
}

/**
 * Writes to a GPIO output port pin
 *
 * @param p_gpio The GPIO port
 * @param pin_number The pin number
 * @param value The value to write
 */
void gpio_write_output_pin( Gpiox_reg_t *const p_gpio, uint8_t const pin_number, uint8_t const value)
{
	// If the value is 1
	if (value) p_gpio->odr |= value << pin_number;
	// Else you clear it
	else p_gpio->odr &= ~(1 << pin_number);
}

/**
 * Writes to a GPIO output port
 *
 * @param p_gpio
 * @param value
 */
void gpio_write_output_port(Gpiox_reg_t *const p_gpio, uint16_t const value)
{
	p_gpio->odr = value;
}

/**
 * Toggle GPIO pin
 * @param p_gpio The GPIO port
 * @param pin_number The pin number
 */
void gpio_toggle_output_pin(  Gpiox_reg_t *const p_gpio, uint8_t const pin_number )
{
	p_gpio->odr ^= 1 << pin_number;
}


//=========================================================
// Utility
//=========================================================

/**
 * Validate a GPIO handler values
 *
 * @param handle the handle to validate
 * @return 1 when the handler is valid, 0 else
 */
uint8_t validate_gpio_handler( Gpiox_handle_t *handle )
{
	uint8_t is_valid = 0;
	Gpiox_pin_config_t *config = & ( handle->pin_config );
	if ( handle->p_gpiox != NULL )
		if ( config->pin_alt_mode >= GPIO_ALT_MODE_AF0 && config->pin_alt_mode <= GPIO_ALT_MODE_AF15)
			if ( config->pin_mode >= GPIO_PM_INPUT && config->pin_mode <= GPIO_PM_ANALOG)
				if ( config->pin_number >= 0 && config->pin_number <= 15 )
					if ( config->pin_output_type >= GPIO_OUTPUT_PUSH_PULL
							&& config->pin_output_type <= GPIO_OUTPUT_OPEN_DRAIN )
						if ( config->pin_pupd >= GPIO_PUPD_NONE
								&& config->pin_pupd <= GPIO_PUPD_RESERVED )
							if ( config->pin_speed >= GPIO_SPEED_LOW
									&& config->pin_speed <= GPIO_SPEED_VHIGH )
								is_valid = 1;
	return is_valid;
}

/**
 * "Factory" to build a pin config
 *
 * Used to prevent bloat from having to init all the struct and focuses on
 * the most important stuff ( aka pin number and pin mode )
 *
 * @param pin_number the pin number
 * @param pin_mode the pin mode
 * @return a pin configuration (low speed, no pupd, pushpull, and alternate function 0)
 */
Gpiox_pin_config_t gpio_build_pin_config(enum GPIO_PIN_NUMBER pin_number, enum GPIO_PORT_MODE pin_mode)
{
	Gpiox_pin_config_t pin_config = { pin_number, pin_mode, GPIO_SPEED_LOW,
			GPIO_PUPD_NONE, GPIO_OUTPUT_PUSH_PULL, GPIO_ALT_MODE_AF0 };
	return pin_config;
}

