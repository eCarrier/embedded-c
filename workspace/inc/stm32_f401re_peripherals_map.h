/*
 * stm32_f401re_peripherals.h
 *
 *  Created on: Jul 13, 2020
 *      Author: etienne
 */

#include <stdint.h>

#ifndef STM32_F401RE_PERIPHERALS_H_
#define STM32_F401RE_PERIPHERALS_H_


/**
 * ===================================================================
 * General stuff
 * ===================================================================
 */

/**
 * Register bound addresses
 */

enum REG_BOUND_ADDR
{
	RCC_ADDR 	= 0x40023800,
	GPIOA_ADDR 	= 0x40020000,
	GPIOB_ADDR 	= 0x40020400,
	GPIOC_ADDR 	= 0x40020800,
	GPIOD_ADDR 	= 0x40020C00,
	GPIOE_ADDR 	= 0x40021000,
	GPIOH_ADDR 	= 0x40021000,
	ADC1_ADDR   = 0x40012000
};

/**
 * ==================================================================
 * Register clock control
 * ==================================================================
 */

// Offset in the rcc offset
enum RCC_ADDR_OFFSET
{
	RCC_CR 			= 0x00,
	RCC_CFGR 		= 0x08,
	RCC_APB2RSTR 	= 0x24,
	RCC_APB2ENR 	= 0x44,
	RCC_AHB1ENR 	= 0x30
};

// MCO1 clock output option
enum MCO1_CLOCK_OUTPUT
{
	MCO1_HSI = 0x00,
	MCO1_LSE = 0x01,
	MCO1_HSE = 0x02,
	MCO1_PLL = 0x03,
};

// Prescaling values
enum MCOX_PRESCALING
{
	PRE_0 = 0x0,
	PRE_2 = 0x4, // /2
	PRE_3 = 0x5, // /3
	PRE_4 = 0x6, // /4
	PRE_5 = 0x7  // /5

};

// Typedef for the RCC ahb enable register (1)
typedef struct
{

	uint32_t gpioaen 	: 1;
	uint32_t gpioben 	: 1;
	uint32_t gpiocen 	: 1;
	uint32_t gpioden 	: 1;
	uint32_t gpioeen 	: 1;
	uint32_t reserved_1 : 2;
	uint32_t gpiohen 	: 1;
	uint32_t reserved_2 : 4;
	uint32_t crcen		: 1;
	uint32_t reserved_3 : 8;
	uint32_t dma1en		: 1;
	uint32_t dma2en		: 1;
	uint32_t reserved_4 : 9;
} Rcc_ahb1enr_t;

// RCC APB2 peripheral reset register
typedef struct
{

	uint32_t tim1rst		: 1; 	//USART6 reset
	uint32_t reserved_0 	: 3;
	uint32_t usart6rst		: 1; 	//USART6 reset
	uint32_t reserved_1 	: 2;
	uint32_t adc1rst 		: 1; 	//ADC interface reset
	uint32_t reserved_2 	: 2;
	uint32_t sdiorst		: 1; 	//SDIO reset
	uint32_t spi1rst		: 1; 	//SPI1 reset
	uint32_t spi4rst		: 1; 	//SPI4RST
	uint32_t syscfgrst		: 1; 	//System configuration controller reset
	uint32_t reserved_3 	: 1;
	uint32_t tim9rst 		: 1;  	//TIM9 reset
	uint32_t tim10rst 		: 1; 	//TIM10 reset
	uint32_t tim11rst 		: 1; 	//TIM11 reset
	uint32_t reserved_4 	: 13;

} Rcc_apb2rstr_t;

// RCCAPB2 peripheral clock enable register
typedef struct
{
	uint32_t tim1en			: 1;
	uint32_t reserved_1		: 3;
	uint32_t usart1en		: 1;
	uint32_t usart6en 		: 1;
	uint32_t reserved_2		: 2;
	uint32_t adc1en 		: 1;
	uint32_t sdioen			: 1;
	uint32_t spi1en			: 1;
	uint32_t spi4en			: 1;
	uint32_t syscfgen		: 1;
	uint32_t reserved_3		: 1;
	uint32_t tim9en			: 1;
	uint32_t tim10en    	: 1;
	uint32_t tim11en     	: 1;
	uint32_t reserved_4		: 13;
} Rcc_apb2enr_t;

// RCC clock control register (RCC_CR)
typedef struct
{
	uint32_t hsion 		: 1;
	uint32_t hsirdy 	: 1;
	uint32_t reserved_3	: 1;
	uint32_t hsitrim 	: 5;
	uint32_t hsical 	: 8;
	uint32_t hseon 		: 1;
	uint32_t hserdy 	: 1;
	uint32_t hsebyp 	: 1;
	uint32_t csson 		: 1;
	uint32_t reserved_2	: 4;
	uint32_t pllon 		: 1;
	uint32_t pllrdy 	: 1;
	uint32_t plli2son 	: 1;
	uint32_t plli2srdy 	: 1;
	uint32_t reserved_1 : 4;
} Rcc_cr_t;

// RCC clock configuration register (RCC_CFGR)
typedef struct
{
	uint32_t sw  : 2;
	uint32_t sws : 2;
	uint32_t hpre : 4;
	uint32_t reserved_1 : 2;
	uint32_t ppre1 : 3;
	uint32_t ppre2 : 3;
	uint32_t rtcpre : 5;
	uint32_t mco1 : 2;
	uint32_t i2ssrc : 1;
	uint32_t mco1pre : 3;
	uint32_t mco2pre : 3;
	uint32_t mco2 : 2;
} Rcc_cfgr_t;


/**
 * ===================================================================
 * General Purpose Input/Ouput peripherals
 * ===================================================================
 */

enum GPIO_ADDR_OFFSET
{
	GPIOX_MODER = 0x0,
	GPIOX_IDR	= 0x10,
	GPIOX_ODR 	= 0x14,
	GPIOX_AFRL	= 0x20,
	GPIOX_AFRH	= 0x24
};

enum GPIO_ALTERNATE_MODE
{
	AF0 = 0x0,
	AF1,
	AF2,
	AF3,
	AF4,
	AF5,
	AF6,
	AF7,
	AF8,
	AF9,
	AF10,
	AF11,
	AF12,
	AF13,
	AF14,
	AF15,

};

// Enum for the GPIO port mode register
enum GPIOX_MODE
{
	MODER_INPUT 	= 0x0,
	MODER_GPOM 		= 0x1,
	MODER_ALTERNATE = 0x2,
	MODER_ANALOG 	= 0x3
};

// Typedef for the GPIO port mode register
typedef struct
{
	uint32_t moder_0	: 2;
	uint32_t moder_1	: 2;
	uint32_t moder_2	: 2;
	uint32_t moder_3	: 2;
	uint32_t moder_4	: 2;
	uint32_t moder_5	: 2;
	uint32_t moder_6	: 2;
	uint32_t moder_7	: 2;
	uint32_t moder_8	: 2;
	uint32_t moder_9	: 2;
	uint32_t moder_10 	: 2;
	uint32_t moder_11 	: 2;
	uint32_t moder_12 	: 2;
	uint32_t moder_13 	: 2;
	uint32_t moder_14 	: 2;
	uint32_t moder_15 	: 2;
} Gpiox_moder_t;

// Typedef for the GPIO output data register
typedef struct
{
	uint32_t odr_0 	: 1;
	uint32_t odr_1 	: 1;
	uint32_t odr_2 	: 1;
	uint32_t odr_3 	: 1;
	uint32_t odr_4 	: 1;
	uint32_t odr_5 	: 1;
	uint32_t odr_6 	: 1;
	uint32_t odr_7 	: 1;
	uint32_t odr_8 	: 1;
	uint32_t odr_9 	: 1;
	uint32_t odr_10 	: 1;
	uint32_t odr_11 	: 1;
	uint32_t odr_12 	: 1;
	uint32_t odr_13 	: 1;
	uint32_t odr_14 	: 1;
	uint32_t odr_15 	: 1;
} Gpiox_odr_t;

// Typedef for the GPIO input data register
typedef struct
{
	uint32_t idr_0 	: 1;
	uint32_t idr_1 	: 1;
	uint32_t idr_2 	: 1;
	uint32_t idr_3 	: 1;
	uint32_t idr_4 	: 1;
	uint32_t idr_5 	: 1;
	uint32_t idr_6 	: 1;
	uint32_t idr_7 	: 1;
	uint32_t idr_8 	: 1;
	uint32_t idr_9 	: 1;
	uint32_t idr_10 	: 1;
	uint32_t idr_11 	: 1;
	uint32_t idr_12 	: 1;
	uint32_t idr_13 	: 1;
	uint32_t idr_14 	: 1;
	uint32_t idr_15 	: 1;
} Gpiox_idr_t;

// Typedef for the GPIO alternate function low register (GPIOx_AFRL)
typedef struct
{
	uint32_t afrl0 : 4;
	uint32_t afrl1 : 4;
	uint32_t afrl2 : 4;
	uint32_t afrl3 : 4;
	uint32_t afrl4 : 4;
	uint32_t afrl5 : 4;
	uint32_t afrl6 : 4;
	uint32_t afrl7 : 4;
} Gpiox_afrl_t;

// Typedef for the GPIO alternate function high register (GPIOx_AFRH)
typedef struct
{
	uint32_t afrh8 	: 4;
	uint32_t afrh9 	: 4;
	uint32_t afrh10 : 4;
	uint32_t afrh11 : 4;
	uint32_t afrh12 : 4;
	uint32_t afrh13 : 4;
	uint32_t afrh14 : 4;
	uint32_t afrh15 : 4;
} Gpiox_afrh_t;



/**
 * ===================================================================
 * Analog-to-digital converter (ADC)
 * ===================================================================
 */

enum ADC_ADDR_OFFSET
{
	ADC_CR1 = 0x04
};

// Analog to digital converter control register 1
typedef struct // ADC control register 1
{
	// Analog watchdog select bit
	uint32_t awdch 		: 5;
	// Interrupt enable for end of conversion (eoc)
	uint32_t eocie 		: 1;
	// Analog watchdog interrupt enable
	uint32_t awdie 		: 1;
	// Injected channels interrupt enable
	uint32_t jeocie 	: 1;
	// Scan mode
	uint32_t scan 		: 1;
	// Enable the watchdog on a single channel in scan mode
	uint32_t awdsgl		: 1;
	// Automatic injected group conversion
	uint32_t jauto		: 1;
	// Discontinuous mode on regular channels
	uint32_t disc_en	: 1;
	// Discontinuous mode on injected channels
	uint32_t jdisc_en	: 1;
	// Discontinuous mode channel count
	uint32_t disc_num	: 3;
	uint32_t reserved_1 : 5;
	// Analog watchdog enable on injected channels
	uint32_t jawden 	: 1;
	// Analog watchdog enable on regular channels
	uint32_t awden 		: 1;
	// Resolution
	uint32_t res 		: 2;
	// Overrun interrupt enable
	uint32_t ovrie 		: 1;
	uint32_t reserved_2 : 5;
} Adc_cr1_t ;



#endif /* F401RE_PERIPHERALS_H_ */
