/**
 * stm32_peripherals_access.h
 *
 * This contains various peripheral access methods for
 * stm32 family microcontronllers:w
 *
 *  Created on: Jul 13, 2020
 *  Author: etienne
 */

#ifndef STM32_PERIPHERALS_ACCESS_H_
#define STM32_PERIPHERALS_ACCESS_H_


#include "stdint.h"

inline void pet(){};

static inline void * const get_memloc(const uint32_t address, const uint32_t offset)
{
	return (void * const)(address + offset);
}


#endif
