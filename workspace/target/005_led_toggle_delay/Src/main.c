/**
 ******************************************************************************
 * @file           : main.c
 * @author         : Auto-generated by STM32CubeIDE
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
#include <stdio.h>
#include <stdint.h>

#if !defined(__SOFT_FP__) && defined(__ARM_FP)
  #warning "FPU is not initialized, but the project is compiling for an FPU. Please initialize the FPU before use."
#endif

int main(void)
{
	int i = 0;
	// Led is on PA5
	// RCC address 0x4002 3800
	uint32_t * const p_rcc 		= (uint32_t *) 0x40023800;
	// GPIOA Control register in the RCC is at +0x30
	const uint8_t rcc_ctr_gpioa = 0x30;
	// GPIOA Register is at 0x40020000
	uint32_t * const p_gpioa 	= (uint32_t *) 0x40020000;
	// GPIOA mode register at +0x00
	const uint8_t gpioa_moder 	= 0x00;
	// GPIOA odr at +0x14
	const uint8_t gpioa_odr 	= 0x14;

	// Enable GPIOA via rcc
	*(p_rcc + rcc_ctr_gpioa/4) |= 0x1 ;
	// Clear the mode register for PA5 and then set it to 0x1
	*(p_gpioa + gpioa_moder/4) &= ~(3 << 10);
	*(p_gpioa + gpioa_moder/4) |= (1 << 10);

	for(;;)
	{
		i = 0;
		// Do 10000 NOPs
		while (i++ < 1000000) ;
		// toggle 1 to odr+0x5
		*(p_gpioa + gpioa_odr/4) ^= (1 << 5);
		printf("Toggling led : ");
	}

}
