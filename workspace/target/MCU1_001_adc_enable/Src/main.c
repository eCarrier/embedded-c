/**
 ******************************************************************************
 * @file           : main.c
 * @author         : Auto-generated by STM32CubeIDE
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include "stm32_f401re_peripherals_map.h"
#include "stm32_peripherals_access.h"
#include "stdio.h"

#if !defined(__SOFT_FP__) && defined(__ARM_FP)
  #warning "FPU is not initialized, but the project is compiling for an FPU. Please initialize the FPU before use."
#endif

int main(void)
{
	// Registers
	Adc_cr1_t volatile * const adc_cr1 = (Adc_cr1_t * const)get_memloc(ADC1_ADDR, ADC_CR1);
	Rcc_apb2enr_t volatile * const apb2enr = (Rcc_apb2enr_t * const)get_memloc(RCC_ADDR, RCC_APB2ENR);
	apb2enr->adc1en = 1;
    printf("%d\n", adc_cr1->scan);
    while (1)
    {
    	adc_cr1->scan = !adc_cr1->scan;
    	printf("%d\n", adc_cr1->scan);
    }
}
