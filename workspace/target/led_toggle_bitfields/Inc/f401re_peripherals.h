/*
 * f401re_peripherals.h
 *
 *  Created on: Jul 13, 2020
 *      Author: etienne
 */

#include <stdint.h>

#ifndef F401RE_PERIPHERALS_H_
#define F401RE_PERIPHERALS_H_

static inline uint32_t read_peripheral_reg (uint32_t addrs, uint32_t offset)
{
	return *((uint32_t *)addrs + offset/4);
}

static inline void set_peripheral (uint32_t addrs, uint32_t offset, uint32_t new_val)
{
	*((uint32_t *)addrs+ offset/4) = new_val;
}


/**
 * ===================================================================
 * General stuff
 * ===================================================================
 */

/**
 * Register bound addresses
 */

enum REG_BOUND_ADDR
{
	RCC_ADDR 	= 0x40023800,
	GPIOA_ADDR 	= 0x40020000,
	GPIOB_ADDR 	= 0x40020400,
	GPIOC_ADDR 	= 0x40020800,
	GPIOD_ADDR 	= 0x40020C00,
	GPIOE_ADDR 	= 0x40021000,
	GPIOH_ADDR 	= 0x40021000
};

/**
 * ==================================================================
 * Register clock control
 * ==================================================================
 */

// Offset in the rcc offset
enum RCC_ADDR_OFFSET
{
	RCC_AHB1ENR = 0x30
};

// Typedef for the RCC ahb enable register (1)
typedef struct {

	uint32_t gpioaen 	: 1;
	uint32_t gpioben 	: 1;
	uint32_t gpiocen 	: 1;
	uint32_t gpioden 	: 1;
	uint32_t gpioeen 	: 1;
	uint32_t reserved_1 : 2;
	uint32_t gpiohen 	: 1;
	uint32_t reserved_2 : 4;
	uint32_t crcen		: 1;
	uint32_t reserved_3 : 8;
	uint32_t dma1en		: 1;
	uint32_t dma2en		: 1;
	uint32_t reserved_4 : 9;

} Rcc_ahb1enr_t;

/**
 * ===================================================================
 * General Purpose Input/Ouput peripherals
 * ===================================================================
 */

enum GPIO_ADDR_OFFSET
{
	GPIOX_MODER = 0x0,
	GPIOX_ODR 	= 0x14,
	GPIOX_IDR	= 0x10
};

// Enum for the GPIO port mode register
enum GPIOX_MODE
{
	MODER_INPUT 	= 0x0,
	MODER_GPOM 		= 0x1,
	MODER_ALTERNATE = 0x2,
	MODER_ANALOG 	= 0x3
};

// Typedef for the GPIO port mode register
typedef struct {
	uint32_t moder_0_0 		: 1;
	uint32_t moder_0_1 		: 1;
	uint32_t moder_1_0 		: 1;
	uint32_t moder_1_1 		: 1;
	uint32_t moder_2_0 		: 1;
	uint32_t moder_2_1 		: 1;
	uint32_t moder_3_0 		: 1;
	uint32_t moder_3_1 		: 1;
	uint32_t moder_4_0 		: 1;
	uint32_t moder_4_1 		: 1;
	uint32_t moder_5_0 		: 1;
	uint32_t moder_5_1 		: 1;
	uint32_t moder_6_0 		: 1;
	uint32_t moder_6_1 		: 1;
	uint32_t moder_7_0 		: 1;
	uint32_t moder_7_1 		: 1;
	uint32_t moder_8_0 		: 1;
	uint32_t moder_8_1 		: 1;
	uint32_t moder_9_0 		: 1;
	uint32_t moder_9_1 		: 1;
	uint32_t moder_10_0 	: 1;
	uint32_t moder_10_1 	: 1;
	uint32_t moder_11_0 	: 1;
	uint32_t moder_11_1 	: 1;
	uint32_t moder_12_0 	: 1;
	uint32_t moder_12_1 	: 1;
	uint32_t moder_13_0 	: 1;
	uint32_t moder_13_1 	: 1;
	uint32_t moder_14_0 	: 1;
	uint32_t moder_14_1 	: 1;
	uint32_t moder_15_0 	: 1;
	uint32_t moder_15_1 	: 1;
} Gpiox_moder_t;

// Typedef for the GPIO output data register
typedef struct {
	uint32_t odr_0 	: 1;
	uint32_t odr_1 	: 1;
	uint32_t odr_2 	: 1;
	uint32_t odr_3 	: 1;
	uint32_t odr_4 	: 1;
	uint32_t odr_5 	: 1;
	uint32_t odr_6 	: 1;
	uint32_t odr_7 	: 1;
	uint32_t odr_8 	: 1;
	uint32_t odr_9 	: 1;
	uint32_t odr_10 	: 1;
	uint32_t odr_11 	: 1;
	uint32_t odr_12 	: 1;
	uint32_t odr_13 	: 1;
	uint32_t odr_14 	: 1;
	uint32_t odr_15 	: 1;
} Gpiox_odr_t;

// Typedef for the GPIO input data register
typedef struct {
	uint32_t idr_0 	: 1;
	uint32_t idr_1 	: 1;
	uint32_t idr_2 	: 1;
	uint32_t idr_3 	: 1;
	uint32_t idr_4 	: 1;
	uint32_t idr_5 	: 1;
	uint32_t idr_6 	: 1;
	uint32_t idr_7 	: 1;
	uint32_t idr_8 	: 1;
	uint32_t idr_9 	: 1;
	uint32_t idr_10 	: 1;
	uint32_t idr_11 	: 1;
	uint32_t idr_12 	: 1;
	uint32_t idr_13 	: 1;
	uint32_t idr_14 	: 1;
	uint32_t idr_15 	: 1;
} Gpiox_idr_t;





#endif /* F401RE_PERIPHERALS_H_ */
